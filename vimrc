set nocompatible              " be iMproved, required
filetype off                  " required

" START - Setting up Vundle - the vim plugin bundler
let iCanHazVundle=1
let vundle_readme=expand('~/.vim/bundle/Vundle.vim/README.md')
if !filereadable(vundle_readme)
  echo "Installing Vundle.."
  echo ""
  silent !mkdir -p ~/.vim/bundle
  silent !git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  let iCanHazVundle=0
endif
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#rc()

" let Vundle manage Vundle, required
" Plugin 'VundleVim/Vundle.vim'
if iCanHazVundle == 0
echo "Installing Bundles, please ignore key map error messages"
echo ""
:PluginInstall
endif
" END - Setting up Vundle - the vim plugin bundler

" set the runtime path to include Vundle and initialize
set rtp+=~/vimfiles/bundle/Vundle.vim
let path='~/vimfiles/bundle'
call vundle#begin(path)

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'bling/vim-airline'
Plugin 'bling/vim-bufferline'
"Plugin 'niklasl/vim-rdf'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-sleuth'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-unimpaired'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-ragtag'
"Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'fholgado/minibufexpl.vim'
"Plugin 'Lokaltog/vim-easymotion'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-shell'
Plugin 'airblade/vim-gitgutter'
"Plugin 'wlangstroth/vim-racket'
"Plugin 'git://git.code.sf.net/p/vim-latex/vim-latex'
Plugin 'luochen1990/rainbow'
Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
Plugin 'drmikehenry/vim-fontsize'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'elzr/vim-json'
Plugin 'geoffharcourt/vim-matchit'
Plugin 'editorconfig/editorconfig-vim'

" Colorschemes
Plugin 'tomasr/molokai'
Plugin 'morhetz/gruvbox'
Plugin 'altercation/vim-colors-solarized'

call vundle#end()

set langmenu=en_US.UTF-8
language messages en_US.UTF-8

" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on
  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!
  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78
  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
  augroup END
else
  set autoindent
endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		\ | wincmd p | diffthis
endif

" Fileformats etc
set fileformats=unix,dos,mac
set foldmethod=syntax		" Use language syntax based folding as default
set encoding=utf-8
set wildignore=.svn;CVS;.git;*.a;*.o;*.class;*.so;*.obj;*.swp;*.jpg;*.png
set wildignorecase

" Turn on completion
set omnifunc=syntaxcomplete#Complete

" CtrlP don't look at my node_modules!
let g:ctrlp_custom_ignore = 'node_modules'

" Allow jsx syntax in .js-files
let g:jsx_ext_required = 0

" Use ESLint for jsx?-linting
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "active_filetypes": [],
    \ "passive_filetypes": [] }

" Override eslint with local version where necessary
let local_eslint = finddir('node_modules', getcwd() . ';') . '/.bin/eslint'
" if matchstr(local_eslint, "^\/\\w") == ''
"   let local_eslint = getcwd() . "/" . local_eslint
" endif
if executable(local_eslint)
  let g:syntastic_javascript_eslint_exec = local_eslint
endif

" For to fix latex under windows
" set shellslash
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
autocmd FileType tex set guioptions+=m

" Swap file locations
set directory=~/vimfiles/.swap//,$HOME/vimfiles/.swap//,.
" Backup file location
set backupdir=~/vimfiles/.bckp//,$HOME/vimfiles/.bckp//,.

set clipboard+=unnamed		" Share clipboard with windows
set laststatus=2		" Always show statusline
color molokai			" Set color scheme for moar pretties
set listchars=tab:▸-,trail:×,extends:>,precedes:<,nbsp:₀
set wrap			" Wrap long lines
set linebreak			" Wrap long lines at breakat characters
set showbreak=\\∙		" Character(s) shown at the start of a wrapped line
set backspace=indent,eol,start	" Allow backspacing over everything

syntax enable			" Enable syntax highliting
set number			" Show line number
set numberwidth=5		" Minimum width of line numbers
set relativenumber		" Show relative line numbers
set ignorecase			" Case insensitive searching
set smartcase			" Case SEnsSITive searching
set hlsearch			" Highlight search matches
set incsearch			" Enable incremental search
set showmatch			" Show matching brace/paren/brackets
set viminfo+=n$HOME/vimfiles/viminfo
set visualbell			" Visual (error) bells
set noerrorbells		" No bells for errors with messages
set mouse=a			" Enable mouse in all modes
set history=1000		" Command history size
set undolevels=1000		" Number of undo levels
set scrolloff=2			" Scroll if cursor is less than this many lines from the edge
set hidden			" Make abandoned buffers hidden instead of unloaded

" Enable rainbow parentheses
"let g:rainbow_conf = {
    "\   'guifgs': [
    "\   '#ffffff','#d7ffff','#afffff','#87ffff','#5fffff','#37ffff','#00ffff',
    "\   '#00ffd7','#00ffaf','#00ff87','#00ff5f','#00ff37','#00ff00'],
    "\}
let g:rainbow_active = 1

" Highlight column for text wrap
set colorcolumn=+0,+1,+2

" " Open NERDTree automagically if no files read on opening vim
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree c:\Users\Mathias\Documents | endif

" Only open MiniBufExplorer manually
let g:miniBufExplorerAutoStart = 0

" Disable GitGutter by default
let g:gitgutter_enabled = 0
" Make gitgutter less eager
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_ViewRule_pdf = 'SumatraPDF'

"" php
let g:php_folding=2

""" Key binds
" Toggle list chars with <leader>l
nnoremap <leader>l :set list!<cr>
" Toggle relative line numbers with <leader>r
nnoremap <leader>r :set rnu!<cr>
" Toggle wrap with <leader>w
nnoremap <leader>w :set wrap!<cr>
" Toggle NERDTree with <leader>n
map <leader>n :NERDTreeToggle<cr>
" Toggle MiniBufExplorer with <leader>m
noremap <leader>m :MBEToggle<cr>
" Toggle GitGutter with <leader>G
noremap <leader>G :GitGutterToggle<cr>
" Toggle Rainbow Parentheses
noremap <leader>R :RainbowToggle<cr>

" Make insert-mode deletions undoable
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>

map [[ ?{<CR>:nohls<CR>w99[{
map ][ /}<CR>:nohls<CR>99]}
map ]] j0[[%/{<CR>
map [] k$][%?}<CR>

" Better wrapping
set linebreak " Wrap on word boundaries
vmap ê gj
vmap ë gk
vmap ° g0
vmap ´ g$
nmap ê gj
nmap ë gk
nmap ° g0
nmap ´ g$

function! ToggleMovement(firstOp, thenOp)
  let pos = getpos('.')
  execute "normal! " . a:firstOp
  if pos == getpos('.')
    execute "normal! " . a:thenOp
  endif
endfunction

" The original carat 0 swap
nnoremap <silent> 0 :call ToggleMovement('^', '0')<CR>

" How about ; and ,
nnoremap <silent> ; :call ToggleMovement(';', ',')<CR>
nnoremap <silent> , :call ToggleMovement(',', ';')<CR>

" nnoremap <silent> G :call ToggleMovement('G', 'gg')<CR>
" nnoremap <silent> gg :call ToggleMovement('gg', 'G')<CR>

" F5 buffer switch
:nnoremap <F5> :buffers<CR>:buffer<Space>
