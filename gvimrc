set mousehide		" Hide the mouse when typing text
" set guifont=Consolas:h10	" Set gui font
set guifont=Fira\ Code\ Light:h12,DejaVu\ Sans\ Mono,Consolas:h10	" Set gui font
set guioptions=gtc

set lines=50			" Editor lines
set columns=150			" Editor columns
winpos 300 150

" Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

set macligatures
set guifont=Fira\ Code:h12
